# Project Tracking

The Tracking is a App for Android, was developed with Kotlin to help a user control all cars. With this App the user can see all Car in a list or in a Map.


## How this project is divided

This project was divided in two Screens:

1. One list, that is the first screen in App, where the user can find all information about the car that is controlled by the App
2. A Map that we can see all car in there geo positions.


## List
The list shows all information about the car. 
Those information are:
* Name
* Address
* Engine Type
* Exterior
* Fuel
* Interior
* VIN Number

When the user select a car in the list, the App show a pin in the Map with the name of the car.

If the user select a button on the Action Bar (Show all on Map), the App show a Map with all cars in the Map. In this case, the name of the car in the pin only will be show when the user select one pin in the Map.

## Architecture
In this project it was used a MVP and the packages was divided this way:
```
 tracking
   |-constants
   |-core
   |-di
   |-feature
     |-listcar
     |-mapcar
   |-model
     |-dao
     |-db
     |-dto
     |-entity
     |-repository
   |-service
     |-rest
```

## Libs
* Dagger2
* Retrofit
* RxJava
* Room

## Language
* Kotlin

## Screenshot
![List](Screenshot_n1.png)
![Map with a selected car](Screenshot_n2.png)
![All car in the map](Screenshot_n3.png)
![All car in the map with a name of car in the pin](Screenshot_n4.png)
