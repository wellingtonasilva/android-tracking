package br.com.wsilva.tracking

import android.content.Intent
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import android.support.test.uiautomator.*
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.notNullValue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import android.support.test.uiautomator.UiSelector
import android.support.test.uiautomator.UiObject
import android.support.test.uiautomator.By


@RunWith(AndroidJUnit4::class)
class TrackingUIAutomatorTest {

    companion object {
        private const val TRACKING_APP_PACKAGE = "br.com.wsilva.tracking"
        private const val LAUNCH_TIMEOUT = 5000L
    }

    private lateinit var mDevice: UiDevice

    @Before
    fun startAppFromHomeScreen() {
        // Initialize UiDevice instance
        mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())

        // Start from the home screen
        mDevice.pressHome()

        // Wait for launcher
        val launcherPackage: String = mDevice.launcherPackageName
        assertThat(launcherPackage, notNullValue())
        mDevice.wait(
                Until.hasObject(By.pkg(launcherPackage).depth(0)),
                LAUNCH_TIMEOUT
        )

        // Launch the app
        val context = InstrumentationRegistry.getContext()
        val intent = context.packageManager.getLaunchIntentForPackage(TRACKING_APP_PACKAGE).apply {
            // Clear out any previous instances
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        }
        context.startActivity(intent)

        // Wait for the app to appear
        mDevice.wait(
                Until.hasObject(By.pkg(TRACKING_APP_PACKAGE).depth(0)),
                LAUNCH_TIMEOUT
        )
    }

    @Test
    fun showAllOnMap() {
        val showAllOnMapMenuOption: UiObject = mDevice.findObject(
                UiSelector().text("Show all on map").className("android.support.v7.widget.LinearLayoutCompat")
        )
        if (showAllOnMapMenuOption.exists()) {
            showAllOnMapMenuOption.click()
        }
    }

    @Test
    fun listOfCarIsNotEmpty() {
        val recyclerView: UiObject = mDevice.findObject(
                UiSelector().className("android.support.v7.widget.RecyclerView")
                        .instance(0)
        )
        assert(recyclerView.exists())
        assert(recyclerView.childCount > 0)
    }

    @Test
    fun existCarNameOnListOfCar() {
        val recyclerView = UiScrollable(
                UiSelector().className("android.support.v7.widget.RecyclerView")
        )
        val layoutContainer: UiObject = recyclerView.getChildByText(
                UiSelector().className("android.widget.LinearLayout"),"HH-GO8003")
        assert(layoutContainer.exists())
    }

    @Test
    fun selectItemOnListOfCar() {
        val recyclerView = UiScrollable(
                UiSelector().className("android.support.v7.widget.RecyclerView")
        )
        val layoutContainer: UiObject = recyclerView.getChildByText(
                UiSelector().className("android.widget.LinearLayout"),"HH-GO8003")
        if (layoutContainer.exists()) {
            layoutContainer.click()
        }

        val markerContentDescription = "HH-GO8003. "
        val marker = mDevice.findObject(By.desc(markerContentDescription))
        assertThat(marker, notNullValue())
    }
}