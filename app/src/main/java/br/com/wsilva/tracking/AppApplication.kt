package br.com.wsilva.tracking

import android.app.Application
import br.com.wsilva.tracking.di.AppComponent
import br.com.wsilva.tracking.di.AppDatabaseModule
import br.com.wsilva.tracking.di.AppModule
import br.com.wsilva.tracking.di.DaggerAppComponent

class AppApplication: Application() {
    companion object
    {
        @JvmStatic lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent
                .builder()
                .appModule(AppModule(this))
                .appDatabaseModule(AppDatabaseModule(this))
                .build()
        appComponent.inject(this)
    }
}