package br.com.wsilva.tracking.constants

class AppConstants {
    companion object {
        val BASE_URL_APP_CLOUD  = "https://s3-us-west-2.amazonaws.com"
        val KEY_CAR_ID: String  = "KEY_CAR_ID"
        val KEY_MAP_VIEW_OPTION = "KEY_MAP_VIEW_OPTION"
        val MAP_VIEW_OPTION_ONLY_ONE = 1
        val MAP_VIEW_OPTION_ALL      = 2
    }
}