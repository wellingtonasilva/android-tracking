package br.com.wsilva.tracking.feature.listcar

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.wsilva.tracking.R
import br.com.wsilva.tracking.model.entity.CarMarkEntity
import kotlinx.android.synthetic.main.lay_car_list_adapter.view.*

class CarListAdapter(private val context: Context,
                      private val list: List<CarMarkEntity>,
                      private val listener: CarListListener) : RecyclerView.Adapter<CarListAdapter.ViewHolder>()
{
    interface CarListListener {
        fun onClickListener(entity: CarMarkEntity)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder
    {
        var view = LayoutInflater.from(context).inflate(R.layout.lay_car_list_adapter, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int)
    {
        val res = context.resources
        val entity = list[position]
        holder.tvwName.text = entity.name
        holder.tvwAddress.text = entity.address
        holder.tvwEngineType.text =  String.format(res.getString(R.string.app_fmt_engine_type), entity.engineType)
        holder.tvwExterior.text = String.format(res.getString(R.string.app_fmt_exterior), entity.exterior)
        holder.tvwFuel.text = String.format(res.getString(R.string.app_fmt_fuel), entity.fuel.toString())
        holder.tvwInterior.text = String.format(res.getString(R.string.app_fmt_interior), entity.interior)
        holder.tvwVin.text = String.format(res.getString(R.string.app_fmt_vin), entity.vin)
        holder.container.setOnClickListener { view -> listener.onClickListener(entity) }
    }

    override fun getItemCount(): Int
    {
        return list.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    {
        val container = itemView.container
        val tvwName = itemView.tvwName
        val tvwAddress = itemView.tvwAddress
        val tvwEngineType = itemView.tvwEngineType
        val tvwExterior = itemView.tvwExterior
        val tvwFuel = itemView.tvwFuel
        val tvwInterior = itemView.tvwInterior
        val tvwVin = itemView.tvwVin
    }
}