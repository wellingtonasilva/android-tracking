package br.com.wsilva.tracking.model.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import br.com.wsilva.tracking.model.dao.CarMarkDAO
import br.com.wsilva.tracking.model.entity.CarMarkEntity

@Database(version = 1, entities = arrayOf(CarMarkEntity::class))
abstract class AppDatabase: RoomDatabase() {
    abstract fun getCarMarkDAO(): CarMarkDAO
}