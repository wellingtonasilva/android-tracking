package br.com.wsilva.tracking.feature.listcar.di

import br.com.wsilva.tracking.di.AppDatabaseModule
import br.com.wsilva.tracking.feature.listcar.CarListFragment
import br.com.wsilva.tracking.feature.listcar.CarListPresenter
import dagger.Component

@Component(modules = arrayOf(CarListModule::class, AppDatabaseModule::class))
interface CarListComponent {
    fun inject(presenter: CarListPresenter)
    fun inject(fragment: CarListFragment)
}