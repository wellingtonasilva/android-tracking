package br.com.wsilva.tracking.di

import android.app.Application
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AppModule::class, AppDatabaseModule::class))
interface AppComponent {
    fun inject(application: Application)
    var appDatabaseModule: AppDatabaseModule
    var application: Application
}