package br.com.wsilva.tracking.feature.listcar.di

import br.com.wsilva.tracking.feature.listcar.CarListContract
import br.com.wsilva.tracking.feature.listcar.CarListFragment
import br.com.wsilva.tracking.feature.listcar.CarListPresenter
import br.com.wsilva.tracking.model.repository.CarMarkRepository
import br.com.wsilva.tracking.service.AppApiService
import dagger.Module
import dagger.Provides

@Module
class CarListModule(private val view: CarListFragment) {
    @Provides
    fun providesCarListView(): CarListContract.View {
        return view
    }

    @Provides
    fun providesCarListPresenter(view: CarListContract.View, apiService: AppApiService,
                                 repository: CarMarkRepository): CarListPresenter {
        return CarListPresenter(view, apiService, repository)
    }
}