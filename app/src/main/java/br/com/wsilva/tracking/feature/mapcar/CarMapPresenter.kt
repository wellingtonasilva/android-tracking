package br.com.wsilva.tracking.feature.mapcar

import br.com.wsilva.tracking.constants.AppConstants
import br.com.wsilva.tracking.model.entity.CarMarkEntity
import br.com.wsilva.tracking.model.repository.CarMarkRepository
import com.google.android.gms.maps.GoogleMap
import javax.inject.Inject
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import com.google.android.gms.maps.model.LatLngBounds

class CarMapPresenter: CarMapContract.Presenter {
    val repository: CarMarkRepository
    val view: CarMapContract.View
    lateinit var googleMap: GoogleMap
    //Car Id selected in the list
    var id: Long = 0
    //This flag is necessary to show all cars on map or only the selected car in the list
    var mapViewOption = AppConstants.MAP_VIEW_OPTION_ONLY_ONE
    var builder = LatLngBounds.Builder()

    @Inject
    constructor(view: CarMapContract.View, repository: CarMarkRepository) {
        this.view = view
        this.repository = repository
    }

    protected fun addMarkAndZoom(title: String, latLng: LatLng, zoom: Float) {
        if (googleMap != null) {
            googleMap.addMarker(MarkerOptions()
                    .position(latLng)
                    .title(title))
                    .showInfoWindow()
            val cameraPosition = CameraPosition.Builder()
                    .target(latLng)
                    .zoom(zoom)
                    .build()
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
        }
    }

    protected fun addMark(title: String, latLng: LatLng) {
        if (googleMap != null) {
            googleMap.addMarker(MarkerOptions()
                    .position(latLng)
                    .title(title))
        }
    }

    override fun addMarks() {
        when (mapViewOption) {
            AppConstants.MAP_VIEW_OPTION_ONLY_ONE -> addSelectedCarMark()
            AppConstants.MAP_VIEW_OPTION_ALL -> addlAllCarMarks()
        }
    }
    
    private fun addSelectedCarMark() {
        Observable.create<CarMarkEntity> { e -> e.onNext(repository.get(id))}
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe { t: CarMarkEntity -> addMarkAndZoom(t.name, LatLng(t.latitude, t.longitude), 17f) }
    }

    private fun addlAllCarMarks() {
        view.showProgress()
        Observable.create<CarMarkEntity> { e ->
                val list = repository.listAll()
                list.forEach { item ->
                    builder.include(LatLng(item.latitude, item.longitude))
                    e.onNext(item)
                }
                e.onComplete()
            }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnComplete { googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 100)); view.dismissProgress()}
                .subscribe { t: CarMarkEntity -> addMark(t.name, LatLng(t.latitude, t.longitude)) }
    }
}