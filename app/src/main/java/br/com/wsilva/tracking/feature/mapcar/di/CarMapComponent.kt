package br.com.wsilva.tracking.feature.mapcar.di

import br.com.wsilva.tracking.di.AppDatabaseModule
import br.com.wsilva.tracking.feature.mapcar.CarMapActivity
import br.com.wsilva.tracking.feature.mapcar.CarMapPresenter
import dagger.Component

@Component(modules = arrayOf(CarMapModule::class, AppDatabaseModule::class))
interface CarMapComponent {
    fun inject(activity: CarMapActivity)
    fun inject(presenter: CarMapPresenter)
}