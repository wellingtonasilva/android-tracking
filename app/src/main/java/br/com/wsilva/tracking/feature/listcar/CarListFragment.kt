package br.com.wsilva.tracking.feature.listcar

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import br.com.wsilva.tracking.AppApplication
import br.com.wsilva.tracking.R
import br.com.wsilva.tracking.constants.AppConstants
import br.com.wsilva.tracking.feature.listcar.di.CarListModule
import br.com.wsilva.tracking.feature.listcar.di.DaggerCarListComponent
import br.com.wsilva.tracking.feature.mapcar.CarMapActivity
import br.com.wsilva.tracking.model.entity.CarMarkEntity
import kotlinx.android.synthetic.main.lay_car_list_fragment.*
import javax.inject.Inject

class CarListFragment: Fragment(), CarListContract.View, CarListAdapter.CarListListener {
    @Inject
    lateinit var presenter: CarListPresenter
    lateinit var progressDialog: ProgressDialog

    companion object {
        val TAG: String = "CarListFragment"

        fun newInstance(): CarListFragment {
            return CarListFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        registerProgress()
        DaggerCarListComponent.builder()
                .carListModule(CarListModule(this))
                .appDatabaseModule(AppApplication.appComponent.appDatabaseModule)
                .build()
                .inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.lay_car_list_fragment, container, false)
        setHasOptionsMenu(true)

        return root
    }

    override fun onResume() {
        super.onResume()
        presenter.loadCar()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_car_list, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.navigation_show_all_on_map -> {
                showAllCarOnMap()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun showCarList(list: List<CarMarkEntity>) {
        val adapter = CarListAdapter(activity!!, list, this)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.adapter = adapter
    }

    override fun onClickListener(entity: CarMarkEntity) {
        showCarOnMap(entity.id)
    }

    override fun registerProgress()
    {
        progressDialog = ProgressDialog(activity)
        progressDialog.isIndeterminate = true
        progressDialog.max = 100
        progressDialog.setTitle(activity!!.baseContext.resources.getText(R.string.app_name))
        progressDialog.setMessage(activity!!.baseContext.resources.getText(R.string.app_aguarde))
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progressDialog.setCancelable(false)
    }

    override fun showProgress() {
        if (progressDialog != null) progressDialog.show()
    }

    override fun dismissProgress() {
        if (progressDialog != null) progressDialog.dismiss()
    }

    override fun showCarOnMap(id: Long) {
        val intent = Intent(activity, CarMapActivity::class.java)
        val bundle = Bundle()
        bundle.putLong(AppConstants.KEY_CAR_ID, id)
        bundle.putInt(AppConstants.KEY_MAP_VIEW_OPTION, AppConstants.MAP_VIEW_OPTION_ONLY_ONE)
        intent.putExtras(bundle)
        startActivity(intent)
    }

    override fun showAllCarOnMap() {
        val intent = Intent(activity, CarMapActivity::class.java)
        val bundle = Bundle()
        bundle.putInt(AppConstants.KEY_MAP_VIEW_OPTION, AppConstants.MAP_VIEW_OPTION_ALL)
        intent.putExtras(bundle)
        startActivity(intent)
    }
}