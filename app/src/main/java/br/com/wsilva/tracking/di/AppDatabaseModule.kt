package br.com.wsilva.tracking.di

import android.app.Application
import android.arch.persistence.room.Room
import br.com.wsilva.tracking.constants.AppConstants
import br.com.wsilva.tracking.model.dao.CarMarkDAO
import br.com.wsilva.tracking.model.db.AppDatabase
import br.com.wsilva.tracking.model.repository.CarMarkRepository
import br.com.wsilva.tracking.service.AppApiService
import dagger.Module
import dagger.Provides
import javax.inject.Inject

@Module
class AppDatabaseModule {
    companion object {
        @JvmStatic lateinit var appDatabase : AppDatabase
    }

    val application: Application
    val apiService: AppApiService

    @Inject
    constructor(application: Application) {
        this.application = application
        appDatabase = Room.databaseBuilder(application, AppDatabase::class.java, "dabase.db").build()
        apiService = AppApiService(AppConstants.BASE_URL_APP_CLOUD)
    }

    @Provides
    fun application() : Application {
        return application
    }

    @Provides
    fun providesAppDatabase(application: Application) : AppDatabase {
        return appDatabase
    }

    @Provides
    fun providesAppApiService(): AppApiService {
        return AppApiService(AppConstants.BASE_URL_APP_CLOUD)
    }

    @Provides
    fun providesProdutoDAO(): CarMarkDAO
    {
        return appDatabase.getCarMarkDAO()
    }

    @Provides
    fun providesProdutoRepository(dao: CarMarkDAO): CarMarkRepository
    {
        return CarMarkRepository(dao)
    }
}