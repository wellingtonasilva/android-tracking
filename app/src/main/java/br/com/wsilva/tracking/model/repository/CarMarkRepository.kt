package br.com.wsilva.tracking.model.repository

import br.com.wsilva.tracking.core.BasicRepository
import br.com.wsilva.tracking.model.dao.CarMarkDAO
import br.com.wsilva.tracking.model.dto.PlaceMarsks
import br.com.wsilva.tracking.model.entity.CarMarkEntity
import io.reactivex.Observable

class CarMarkRepository(private val dao:CarMarkDAO): BasicRepository<CarMarkEntity>() {
    override fun listAll(): List<CarMarkEntity> {
        return dao.listAll()
    }

    override fun get(id: Long): CarMarkEntity {
        return dao.get(id)
    }

    override fun delete(entity: CarMarkEntity): Int {
        return dao.delete(entity)
    }

    override fun deleteAll() {
        return dao.deleteAll()
    }

    override fun insert(entity: CarMarkEntity): Long {
        return dao.insert(entity)
    }

    override fun update(entity: CarMarkEntity): Int {
        return dao.update(entity)
    }

    override fun count(): Int {
        return dao.count()
    }

    fun saveCarMark(marks: PlaceMarsks): Observable<Boolean> {
        return Observable.create { e ->
            marks.placemarks.forEach { item ->
                insert(CarMarkEntity(name = item.name, address = item.address,
                        engineType = item.engineType, exterior = item.exterior,
                        fuel = item.fuel, interior = item.interior,
                        latitude = item.coordinates[1], longitude = item.coordinates[0], vin = item.vin))
            }
            e.onNext(true)
        }
    }

    fun loadCarMar(): Observable<List<CarMarkEntity>> {
        return Observable.create { e -> e.onNext(listAll())}
    }
}