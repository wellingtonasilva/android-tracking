package br.com.wsilva.tracking.model.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "car_mark")
data class CarMarkEntity(@ColumnInfo(name = "address") var address: String,
                         @ColumnInfo(name = "engineType") var engineType: String,
                         @ColumnInfo(name = "exterior") var exterior: String,
                         @ColumnInfo(name = "fuel") var fuel: Int,
                         @ColumnInfo(name = "interior") var interior: String,
                         @ColumnInfo(name = "name") var name: String,
                         @ColumnInfo(name = "vin") var vin: String,
                         @ColumnInfo(name = "coordinates_latitude") var latitude: Double,
                         @ColumnInfo(name = "coordinates_longitude") var longitude: Double
                      )
{
    @ColumnInfo(name = "_id")
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}