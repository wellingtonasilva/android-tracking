package br.com.wsilva.tracking.feature.mapcar

import android.app.ProgressDialog
import android.os.Bundle
import android.view.MenuItem
import br.com.wsilva.tracking.AppApplication
import br.com.wsilva.tracking.R
import br.com.wsilva.tracking.constants.AppConstants
import br.com.wsilva.tracking.core.BasicActivity
import br.com.wsilva.tracking.feature.mapcar.di.CarMapModule
import br.com.wsilva.tracking.feature.mapcar.di.DaggerCarMapComponent
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import javax.inject.Inject

class CarMapActivity: BasicActivity(), OnMapReadyCallback, CarMapContract.View {

    @Inject
    lateinit var presenter: CarMapPresenter
    lateinit var progressDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //DI
        DaggerCarMapComponent.builder()
                .carMapModule(CarMapModule(this))
                .appDatabaseModule(AppApplication.appComponent.appDatabaseModule)
                .build()
                .inject(this)

        //Set layout
        setContentView(R.layout.lay_car_map_activity)

        //Create a progress
        registerProgress()

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        //Obtain parameters
        if (savedInstanceState == null) {
            presenter.mapViewOption = intent.extras.getInt(AppConstants.KEY_MAP_VIEW_OPTION)
            presenter.id = intent.extras.getLong(AppConstants.KEY_CAR_ID, 0)
        } else {
            presenter.mapViewOption = savedInstanceState.getInt(AppConstants.KEY_MAP_VIEW_OPTION)
            presenter.id = savedInstanceState.getLong(AppConstants.KEY_CAR_ID, 0)
        }

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        presenter.googleMap = googleMap
        presenter.addMarks()
    }

    override fun registerProgress()
    {
        progressDialog = ProgressDialog(this)
        progressDialog.isIndeterminate = true
        progressDialog.max = 100
        progressDialog.setTitle(resources.getText(R.string.app_name))
        progressDialog.setMessage(resources.getText(R.string.app_aguarde))
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progressDialog.setCancelable(false)
    }

    override fun showProgress() {
        if (progressDialog != null) progressDialog.show()
    }

    override fun dismissProgress() {
        if (progressDialog != null) progressDialog.dismiss()
    }
}