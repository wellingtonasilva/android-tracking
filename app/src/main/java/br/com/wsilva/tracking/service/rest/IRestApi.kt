package br.com.wsilva.tracking.service.rest

import br.com.wsilva.tracking.model.dto.PlaceMarsks
import io.reactivex.Observable
import retrofit2.http.GET

interface IRestApi {
    @GET("/wunderbucket/locations.json")
    fun getPlackemarks(): Observable<PlaceMarsks>
}