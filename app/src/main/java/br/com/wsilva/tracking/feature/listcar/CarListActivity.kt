package br.com.wsilva.tracking.feature.listcar

import android.os.Bundle
import br.com.wsilva.tracking.R
import br.com.wsilva.tracking.core.BasicActivity

class CarListActivity: BasicActivity() {
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.lay_car_list_activity)
        //Configurão inciial
        init(savedInstanceState)
    }

    fun init(savedInstanceState: Bundle?)
    {
        val fragmentManager = supportFragmentManager
        var fragment = fragmentManager.findFragmentByTag(CarListFragment.TAG)
        if (fragment == null) {
            fragment = CarListFragment.newInstance()
        }
        fragment.arguments = savedInstanceState
        addFragmentToActivity(fragmentManager, fragment, R.id.frameLayout, CarListFragment.TAG)
    }
}