package br.com.wsilva.tracking.model.dao

import android.arch.persistence.room.*
import br.com.wsilva.tracking.model.entity.CarMarkEntity

@Dao
interface CarMarkDAO {
    @Query("SELECT * FROM car_mark")
    fun listAll(): List<CarMarkEntity>

    @Query("SELECT * FROM car_mark WHERE _id = :id")
    fun get(id: Long) : CarMarkEntity

    @Delete
    fun delete(entity: CarMarkEntity) : Int

    @Query("DELETE FROM car_mark")
    fun deleteAll()

    @Insert
    fun insert(entity: CarMarkEntity) : Long

    @Update
    fun update(entity: CarMarkEntity) : Int

    @Query("SELECT COUNT(1) FROM car_mark")
    fun count(): Int
}