package br.com.wsilva.tracking.model.dto

import com.google.gson.annotations.SerializedName

data class CarMarkDTO(@SerializedName("address") val address: String,
                      @SerializedName("engineType") val engineType: String,
                      @SerializedName("exterior") val exterior: String,
                      @SerializedName("fuel") val fuel: Int,
                      @SerializedName("interior") val interior: String,
                      @SerializedName("name") val name: String,
                      @SerializedName("vin") val vin: String,
                      @SerializedName("coordinates") val coordinates: ArrayList<Double>
                      )
{
}