package br.com.wsilva.tracking.di

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides

@Module
class AppModule(val application: Application) {
    @Provides
    fun providesApplication(): Context {
        return application
    }
}