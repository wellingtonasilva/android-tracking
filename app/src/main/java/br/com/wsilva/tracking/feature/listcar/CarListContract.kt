package br.com.wsilva.tracking.feature.listcar

import br.com.wsilva.tracking.core.BasicPresenter
import br.com.wsilva.tracking.model.entity.CarMarkEntity

interface CarListContract {
    interface View {
        fun showCarList(list: List<CarMarkEntity>)
        fun showCarOnMap(id: Long)
        fun showAllCarOnMap()
        fun registerProgress()
        fun showProgress()
        fun dismissProgress()
    }

    interface Presenter: BasicPresenter {
        fun loadCar()
    }
}