package br.com.wsilva.tracking.service

import br.com.wsilva.tracking.model.dto.PlaceMarsks
import br.com.wsilva.tracking.service.rest.IRestApi
import com.google.gson.GsonBuilder
import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class AppApiService(private val baseUrl: String) {
    companion object {
        val TAG = "AppApiService"
    }

    fun getRetrofit(): Retrofit {
        val gson = GsonBuilder()
                .enableComplexMapKeySerialization()
                .serializeNulls()
                .setPrettyPrinting()
                .setVersion(1.0)
                .create()

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder()
                .addInterceptor(interceptor)
                //.cache(new Cache(cacheDir, cacheSize))
                .readTimeout(1, TimeUnit.MINUTES)
                .build()

        return Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
    }

    fun getPlackemarks(): Observable<PlaceMarsks> {
        val api = getRetrofit().create(IRestApi::class.java)
        return api.getPlackemarks()
    }
}