package br.com.wsilva.tracking.feature.mapcar.di

import br.com.wsilva.tracking.feature.mapcar.CarMapActivity
import br.com.wsilva.tracking.feature.mapcar.CarMapContract
import br.com.wsilva.tracking.feature.mapcar.CarMapPresenter
import br.com.wsilva.tracking.model.repository.CarMarkRepository
import dagger.Module
import dagger.Provides

@Module
class CarMapModule(private val view: CarMapActivity) {
    @Provides
    fun providesCarMapView(): CarMapContract.View {
        return view
    }

    @Provides
    fun providesCarMapPresenter(view: CarMapContract.View, repository: CarMarkRepository): CarMapContract.Presenter {
        return CarMapPresenter(view, repository)
    }
}