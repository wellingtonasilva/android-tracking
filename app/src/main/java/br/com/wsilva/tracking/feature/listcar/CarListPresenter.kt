package br.com.wsilva.tracking.feature.listcar

import br.com.wsilva.tracking.model.dto.PlaceMarsks
import br.com.wsilva.tracking.model.entity.CarMarkEntity
import br.com.wsilva.tracking.model.repository.CarMarkRepository
import br.com.wsilva.tracking.service.AppApiService
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CarListPresenter: CarListContract.Presenter {
    val view: CarListContract.View
    val apiService: AppApiService
    val carMarkRepository: CarMarkRepository

    @Inject
    constructor(view: CarListContract.View, apiService: AppApiService, carMarkRepository: CarMarkRepository) {
        this.view = view
        this.apiService = apiService
        this.carMarkRepository = carMarkRepository
    }

    override fun loadCar() {
        view.showProgress()
        Observable.create<Boolean> { e -> e.onNext(carMarkRepository.count() > 0)}
                .flatMap { t -> if (t) carMarkRepository.loadCarMar() else loadRemote()}
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .onErrorReturn { arrayListOf() }
                .subscribe { t: List<CarMarkEntity> -> view.dismissProgress(); view.showCarList(t) }
    }

    fun loadRemote(): Observable<List<CarMarkEntity>> {
        return apiService
                .getPlackemarks()
                .flatMap { placeMarsks: PlaceMarsks ->  carMarkRepository.saveCarMark(placeMarsks)}
                .flatMap { carMarkRepository.loadCarMar()}
    }
}