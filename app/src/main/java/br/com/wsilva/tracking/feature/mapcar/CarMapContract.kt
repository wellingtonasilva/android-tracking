package br.com.wsilva.tracking.feature.mapcar

import br.com.wsilva.tracking.core.BasicPresenter

interface CarMapContract {
    interface View {
        fun registerProgress()
        fun showProgress()
        fun dismissProgress()
    }

    interface Presenter: BasicPresenter {
        fun addMarks()
    }
}