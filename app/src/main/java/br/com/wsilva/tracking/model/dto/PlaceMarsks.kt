package br.com.wsilva.tracking.model.dto

import com.google.gson.annotations.SerializedName

data class PlaceMarsks(@SerializedName("placemarks") val placemarks: ArrayList<CarMarkDTO>)
{
}